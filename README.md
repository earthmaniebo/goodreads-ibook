### Setup
1. Install virtualenv and pip
2. run `virtualenv venv`
3. run `. venv/bin/activate` or `source venv/bin/activate`
4. run `pip install -r requirements.txt`
5. run `python main.py`
6. ???
7. profit